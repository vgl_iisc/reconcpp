ReCon - A fast algorithm to compute Reeb graphs
----------------------------------------
This folder contains the following files / folders:
1. recon executable file (compiled on a 64-bit linux machine)
2. samples folder
3. COPYRIGHT notice and VERSION information
4. and this Readme file

Computing Reeb graphs
--------------------
Usage : 
run the recon executable with the following parameters
$ ./recon <inputFile> <inputFunction> <useAdj> [output]
* inputFile - It should be a binary file (format explained below)
* inputFunction - Input function should be 0, or i, where i is the co-ordinate index (to be used for the height function along the ith axis)
* useAdj - 0 if the adjacencies should *not* be stored, and 1 otherwise. Use 1 in order to get best performance, and 0 in order to save memory used.
* output - The output file (optional) to store the Reeb graph.

Input
------
The library currently only supports the following binary (little endian) format:

BIN
***
1. Integer (4-bytes) specifying number of vertices (nv).
2. Integer (4-bytes) specifying dimension (d) the input is embeded in (eg. d = 3 for surface and tetraheddral meshes, where vertices are of the specified by 3 coordinates).
3. For each of the nv vertices, 
   (d+1) floats (4-byte) specifying the d coordinates and the scalar function at that vertex. 
   (In case scalar function is not defined, use -1.0)
4. Each simplex is specified as follows
   A triangle in the input -- 4 Integers: 3, v1, v2, v3 
   A tetrahedron in the input -- 5 Integers: 4, v1, v2, v3, v4
   where v1, v2, v3, and v4 are the vertex indices of the vertices that form the triangles or tetrahedra.
5. Integer value of -1, to mention end of simplex list.

Two sample datasets, one 2D and 2 3D, in the above format is present in the samples folder.

In case of any errors
---------------------
For any error, kindly let me (harishd@csa.iisc.ernet.in) know of the error. It would be great if you can provide the input data :-)



